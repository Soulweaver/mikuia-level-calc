const readline = require('readline');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

console.log('\n\\------    Soulweaver\'s Mikuia.tv Level Calculator    ------/\n');

askQ();

function askQ(){
	rl.question('What level do you want to calculate? ', (level) => {
		console.log(`Calculating level: ${level}`);
		if(level > 0){
			const xp = (((level * 20) * level * 0.8) + level * 100) - 16;
			
			const time = xp / 5 / 60;
			console.log(`Required XP needed for level ${level} is ${xp.toLocaleString()}`);
			console.log(`Required hours needed for level ${level} (based on 5XP every minute) is ${roundToTwo(time)}\n\n`);
			
			askQ();
		} else {
			console.log('Enter a number above 0. kthx\n');
			askQ();
		}
	});
}

//Lazy function, fixes weird issues with numbers like 1.005
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}